import 'dart:convert';

import 'package:api_integ/login.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:http/http.dart' as http;

class ListScreen extends StatefulWidget {
  const ListScreen({Key? key}) : super(key: key);

  @override
  State<ListScreen> createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {
  bool isLoading = false;
  var dataList = [];
  var link = Uri.parse(
      "https://www.pakistanibazaar.pk/Home/GetSearchItem?&ProvinceIdNewSearch=&CityIdNewSearch=&AreaIdNewSearch=&CategorySearchId=1&ItemURLSearch=&pageIndex=1&pageSize=5");
  getData() async {
    setState(() {
      isLoading = true;
    });
    try {
      var response = await http.get(link);
      // print(jsonDecode(response.body));
      setState(() {
        dataList = jsonDecode(response.body);
        setState(() {
          isLoading = false;
        });
      });
      print(dataList);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Pakistani Bazar"),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                );
              },
              icon: const Icon(Icons.login)),
          IconButton(
              onPressed: () {
                getData();
              },
              icon: const Icon(Icons.refresh))
        ],
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Center(
                  child: Text(
                    "Pakistani Bazar",
                    style: TextStyle(
                        fontSize: 22,
                        color: Colors.blue,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    getData();
                  },
                  child: const Text("Get Data"),
                ),
                const SizedBox(
                  height: 20,
                ),
                Expanded(
                  child: isLoading
                      ? const Center(
                          child: CircularProgressIndicator(),
                        )
                      : ListView.builder(
                          itemCount: dataList.length,
                          itemBuilder: ((context, index) {
                            return Card(
                              child: ListTile(
                                leading: Text(
                                    dataList[index]["ProvinceName"].toString()),
                                trailing:
                                    Text(dataList[index]["TimeAgo"].toString()),
                                subtitle: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                        dataList[index]["CityName"].toString()),
                                    Text(
                                        "Price: ${dataList[index]["ItemPrice"].toString()}"),
                                  ],
                                ),
                              ),
                            );
                          }),
                        ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
