import 'dart:convert';

import 'package:api_integ/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'package:http/http.dart' as http;

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var email = TextEditingController(text: "03145355399");

  var password = TextEditingController();
  var parsedLink = Uri.parse("https://www.pakistanibazaar.pk/Home/SignInApp?Email=03145355399Password=pakistan1");
  login() async {
    try {
      var response = await http.post(parsedLink, body: {"Email": email.text, "Password": password.text});

      print(response.body);
      var data = jsonDecode(response.body);
      if (data["FullName"] == "Hanif Khan") {
        Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Login Failed \n $data")));
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Login"),
        actions: [],
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextField(
                  controller: email,
                  decoration: InputDecoration(hintText: "Enter Phone number"),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextField(
                  controller: password,
                  decoration: InputDecoration(hintText: "Enter your password"),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  onPressed: () {
                    print("Button Clikced");
                    login();
                  },
                  child: const Text("Login"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
